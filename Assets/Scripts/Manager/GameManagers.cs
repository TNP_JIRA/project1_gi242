﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

public class GameManagers : Singleton<GameManagers>
{
    
    [SerializeField] private Button startButton,startButton2,restartButton,quitButton,tipButton,CloseTipButton;
    [SerializeField] private Image tipTab;
    [SerializeField] private RectTransform dialog;
    [SerializeField] private RectTransform endDialog;
    [SerializeField] private PlayerSpaceship playerSpaceship;
    [SerializeField] private EnemySpaceship enemySpaceship;
    [SerializeField] private BossSpaceship bossSpaceship;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] float respawnRate = 10f;
    [SerializeField] private AudioClip startButtonSound;
    [SerializeField] private float startButtonVolume = 0.2f;
    [SerializeField] private AudioClip defaultButtonSound;
    [SerializeField] private float defaultButtonVolume = 0.2f;
    private int amountEnemy = 0;
    private int totalScore = 0;
    float respawnEnemy = 0;
    
    
    public event Action OnRestarted;

    private void Start()
    {
        Application.targetFrameRate = 60;
    }

    private void Awake()
    {
        Debug.Assert(tipButton != null, "tipButton cannot be null");
        Debug.Assert(tipTab != null, "tipButton cannot be null");
        Debug.Assert(startButton != null, "startButton cannot be null");
        Debug.Assert(startButton2 != null, "startButton cannot be null");
        Debug.Assert(dialog != null, "dialog cannot be null");
        Debug.Assert(playerSpaceship != null, "playerSpaceShip cannot be null");
        Debug.Assert(enemySpaceship != null, "enemySpaceShip cannot be null");
        Debug.Assert(bossSpaceship != null, "bossSpaceShip cannot be null");
        Debug.Assert(scoreManager != null,"scoreManager cannot be null");
        Debug.Assert(quitButton != null,"quitButton cannot be null");
        Debug.Assert(restartButton != null,"restartButton cannot be null");
        endDialog.gameObject.SetActive(false);
        tipTab.gameObject.SetActive(false);
        startButton.onClick.AddListener(OnStartButtonClicked);
        restartButton.onClick.AddListener(OnRestartButtonClicked);
        quitButton.onClick.AddListener(OnQuitButtonClicked);
        tipButton.onClick.AddListener(OnTipButtonClicked);
        CloseTipButton.onClick.AddListener(OnCloseTipTabClicked);
    }

    private void Update()
    {
        RespawnEnemy();
    }

    private void OnTipButtonClicked()
    {
        tipTab.gameObject.SetActive(true);
        AudioSource.PlayClipAtPoint(defaultButtonSound,Camera.main.transform.position,defaultButtonVolume);
    }

    private void OnCloseTipTabClicked()
    {
        tipTab.gameObject.SetActive(false);
        AudioSource.PlayClipAtPoint(defaultButtonSound,Camera.main.transform.position,defaultButtonVolume);
    }

    private void OnQuitButtonClicked()
    {
        endDialog.gameObject.SetActive(false);
        dialog.gameObject.SetActive(true);
        scoreManager.SetScore(0);
        totalScore = 0;
        amountEnemy = 0;
        AudioSource.PlayClipAtPoint(defaultButtonSound,Camera.main.transform.position,defaultButtonVolume);
    }
    private void OnRestartButtonClicked()
    {
        endDialog.gameObject.SetActive(false);
        RestartGame();
        amountEnemy = 0;
        AudioSource.PlayClipAtPoint(defaultButtonSound,Camera.main.transform.position,defaultButtonVolume);
    }
    

    private void OnStartButtonClicked()
    {
        dialog.gameObject.SetActive(false);
        tipButton.gameObject.SetActive(false);
        StartGame();
        AudioSource.PlayClipAtPoint(startButtonSound,Camera.main.transform.position,startButtonVolume);
    }

    private void RestartGame()
    {
        DestroyRemainingShips();
        scoreManager.SetScore(0);
        totalScore = 0;
        SpawnEnemySpaceship();
        SpawnPlayerSpaceship();
    }

    private void DestroyRemainingShips()
    {
        var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
        foreach (var enemy in remainingEnemies)
        {
            Destroy(enemy);
            
        }

        foreach (var player in remainingEnemies)
        {
            Destroy(player);
        }
    }

    private void StartGame()
    {
        DestroyRemainingShips();
        scoreManager.Init(this);
        SpawnPlayerSpaceship();
        SpawnEnemySpaceship();
    }
    private void SpawnPlayerSpaceship()
    {
        var spaceship = Instantiate(playerSpaceship);
        spaceship.Init(100, 50f);
        spaceship.OnExploded += OnPlayerSpaceshipExploded;
    }

    private void OnPlayerSpaceshipExploded()
    {
        endDialog.gameObject.SetActive(true);
    }

    private void SpawnEnemySpaceship()
    {
        var spaceship = Instantiate(enemySpaceship);
        spaceship.Init(100, 25f);
        spaceship.OnExploded += OnEnemySpaceshipExploded;
    }
    private void SpawnBossSpaceship()
    {
        var spaceship = Instantiate(bossSpaceship);
        spaceship.Init(200, 25f);
        spaceship.OnExploded += OnEnemySpaceshipExploded;
    }

    private void OnEnemySpaceshipExploded()
    {
        totalScore += 1;
        scoreManager.SetScore(totalScore);
        
    }
    
    private void OnBossSpaceshipExploded()
    {
        totalScore += 5;
        scoreManager.SetScore(totalScore);
        
    }

    private void RespawnEnemy()
    {
        respawnEnemy += Time.smoothDeltaTime;
        if (respawnEnemy >= respawnRate)
        {
            SpawnEnemySpaceship();
            respawnEnemy = 0;
            amountEnemy += 1;
        }

        if (amountEnemy == 10)
        {
            SpawnBossSpaceship();
            amountEnemy = 0;
        }
    }
}
