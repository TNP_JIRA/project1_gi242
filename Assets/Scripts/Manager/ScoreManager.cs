﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField] private TextMeshProUGUI scoreText;
    private GameManagers gameManager;

    private void Awake()
    {
        Debug.Assert(scoreText != null, "scoreText cannot be null");
    }

    public void Init(GameManagers gameManager)
    {
        this.gameManager = gameManager;
        this.gameManager.OnRestarted += OnRestarted;
        HideScore(false);
        SetScore(0);
    }

    public void SetScore(int score)
    {
        scoreText.text = $"Score {score}";
        
    }

    public void OnRestarted()
    {
        gameManager.OnRestarted -= OnRestarted;
        HideScore(true);
        SetScore(0);
    }

    private void HideScore(bool hide)
    {
        scoreText.gameObject.SetActive(!hide);
    }
}
