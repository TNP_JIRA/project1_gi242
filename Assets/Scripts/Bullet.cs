﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D rb2D;

    public void Init(Vector2 direction)
    {
        Move(direction);
    }

    private void Awake()
    {
        Debug.Assert(rb2D != null,"Rigitbody2D cannot be null");
    }

    private void Move(Vector2 direction)
    {
        rb2D.velocity = direction * speed;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var target = other.gameObject.GetComponent<IDamagable>();
        target?.TakeHit(damage);
        
        //Destroy(gameObject);
    }
}
