﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PlayerLoop;

public class RestartButton : MonoBehaviour
{
    [SerializeField] private Button restartButton;
    private GameManagers gameManager;
    private ScoreManager scoreManager;
    
    private void Awake()
    {
        Debug.Assert(restartButton != null, "restartButton cannot be null");
    }

    
    public void Init(GameManagers gameManager)
    {
        this.gameManager = gameManager;
        this.gameManager.OnRestarted += OnRestarted;
        HideRestart(true);
        scoreManager.SetScore(0);
    }
    

    public void OnRestarted()
    {
        gameManager.OnRestarted -= OnRestarted;
        HideRestart(false);
    }

    private void HideRestart(bool hide)
    {
        restartButton.gameObject.SetActive(!hide);
    }
}
