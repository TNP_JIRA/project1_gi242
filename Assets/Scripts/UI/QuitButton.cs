﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PlayerLoop;

public class QuitButton : MonoBehaviour
{
    [SerializeField] private Button quitButton;
    private GameManagers gameManager;
    private ScoreManager scoreManager;
    
    private void Awake()
    {
        Debug.Assert(quitButton != null, "restartButton cannot be null");
    }

    
    public void Init(GameManagers gameManager)
    {
        this.gameManager = gameManager;
        this.gameManager.OnRestarted += OnRestarted;
        HideQuit(true);
        scoreManager.SetScore(0);
    }
    

    public void OnRestarted()
    {
        gameManager.OnRestarted -= OnRestarted;
        HideQuit(false);
    }

    private void HideQuit(bool hide)
    {
        quitButton.gameObject.SetActive(!hide);
    }
}
