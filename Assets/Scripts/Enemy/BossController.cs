﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

namespace Enemy
{ 
    public class BossController : MonoBehaviour
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] private BossSpaceship bossSpaceship;
        [SerializeField] private float enemyShipSpeed = 5;
        private float distanceOfTarget = 2.0f;

        private void FixedUpdate()
        {
            bossSpaceship.TripleFire();
        }
    }
}

