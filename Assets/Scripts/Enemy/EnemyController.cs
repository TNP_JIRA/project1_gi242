﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;


public class EnemyController : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private EnemySpaceship enemySpaceship;
    [SerializeField] private float enemyShipSpeed = 5;
    private float distanceOfTarget = 2.0f;


    private void FixedUpdate()
    {
        MoveToPlayer();
        enemySpaceship.Fire();
    }
        
    private bool intersectAABB(Bounds a, Bounds b)
    {
        return ((a.min.x <= b.max.x && a.max.x >= b.min.x) && 
                (a.min.y <= b.max.y && a.max.y >= b.min.y));
    }

    private void MoveToPlayer()
    {
        Vector2 displacementFromTarget = (targetTransform.position - transform.position);
        Vector2 directionToTarget = displacementFromTarget.normalized;
        Vector2 velocity = directionToTarget * enemyShipSpeed;

        float distanceToTarget = displacementFromTarget.magnitude;

        if (distanceToTarget > distanceOfTarget)
        {
            transform.Translate(velocity * Time.deltaTime);
        }
    }
}
