﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Spaceship
{
    public class BossSpaceship : BaseSpaceship,IDamagable
    {
        [SerializeField] private float enemyFireRate = 1f;
        private float fireCounter = 0;
        public event Action OnExploded;

        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExplodeVolume = 0.2f;


        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }

            Exploded();
        }

        public void Exploded()
        {
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(enemyExplodeSound, Camera.main.transform.position, enemyExplodeVolume);
        }

        public override void TripleFire()
        {
            base.TripleFire();
            fireCounter += Time.smoothDeltaTime;
            if (fireCounter >= enemyFireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                var bullet2 = Instantiate(defaultBullet, gunPosition2.position, Quaternion.identity);
                var bullet3 = Instantiate(defaultBullet, gunPosition3.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                bullet2.Init(Vector2.down);
                bullet3.Init(Vector2.down);
                fireCounter = 0;
                
            }
        }
    }
}
