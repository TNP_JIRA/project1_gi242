﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class BaseSpaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected Transform gunPosition2;
        [SerializeField] protected Transform gunPosition3;
    
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        protected virtual void Init(int hp, float speed, Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }

        public virtual void Fire()
        {
            
        }
        public virtual void TripleFire()
        {
            
        }
    }
}

