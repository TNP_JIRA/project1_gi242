﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using Spaceship;
using UnityEngine;
using UnityEngine.Experimental.AI;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship,IDamagable
    {
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerExplodeVolume = 0.2f;


        public event Action OnExploded;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null , "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null,"gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            base.Fire();
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }
        
        public override void TripleFire()
        {
            base.TripleFire();
            var bullet  = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            var bullet2 = Instantiate(defaultBullet, gunPosition2.position, Quaternion.identity);
            var bullet3 = Instantiate(defaultBullet, gunPosition3.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            bullet2.Init(Vector2.up);
            bullet3.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }

            Exploded();
        }

        public void Exploded()
        {
            Debug.Assert(Hp <= 0,"Hp is more than zero");
            Debug.Log("You are dead rip x_x ");
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(playerExplodeSound,Camera.main.transform.position,playerExplodeVolume);
        }
    }
}

