﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float playerShipSpeed = 10;

    private Vector2 movementInput = Vector2.zero;
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 15;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        //movementInput = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
        //movementInput = new Vector2(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("Vertical"));
        //movementInput = movementInput.normalized;
        
        Debug.Log(movementInput);
        
        var newX =  transform.position.x + movementInput.x*Time.deltaTime * playerShipSpeed;
        var newY =  transform.position.y + movementInput.y*Time.deltaTime * playerShipSpeed;
        transform.position = new  Vector2(newX, newY);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }
}
